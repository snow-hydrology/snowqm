################################################################################
############################# INNER FUNCTIONS ##################################
################################################################################
# Internal function calling the cpp function computing seasonal means, 
# returned in a list
compute.data.season.mean <- function(data) {

  seasons <- c("Winter","Spring","Summer","Fall")
  seasons.months <- list("Winter"=c(12,1,2),"Spring"=c(3,4,5),
                         "Summer"=c(6,7,8),"Fall"=c(9,10,11))

  #Copy all the vectobject properties of input data to out, except the data itself
  out <- data
  out$data <- NULL

  for(season in seasons){
    ## Get indices for the season only
    times <- which(lubridate::month(as.POSIXct(data$time.date)) %in% seasons.months[[season]])
    out$data <- data$data[,times]

    years <- lubridate::year(as.POSIXct(data$time.date[times])) +
      ifelse(lubridate::month(as.POSIXct(data$time.date[times]))==12,1,0)

    indices <- list()
    for(yr in unique(years)){
      indices[[paste(yr)]]=which(years==yr)
    }

    #Compute the mean
    out[[season]] <-
      computeSeasonnalSummaryCpp(out$data, fun = "mean.snow", indices = indices)
    out$data <- NULL
  }
  return(out)
}


################################################################################
############################# SINGLE MODEL RUN #################################
################################################################################

#' plt.seasonnal.summary.maps
#'
#' \code{plt.seasonnal.summary.maps} produces plots of maps of seasonal 
#' averaged bias over the whole area covered by the dataset. Plots are not
#' written to a file. This function can be encapsulated into 
#' [plt.seasonnal.summary.details()])
#'
#' @param data.training The training dataset (a vectdata object, see
#' [vectorize.list()])
#' @param data.model The model dataset (a vectdata object, see
#' [vectorize.list()])
#' @param data.corrected the corrected dataset (a vectdata object, see
#' [vectorize.list()])
#' 
#' @return A list with entries "model" and "corrected" containing each a list
#' of the four seasons. Seasonal lists contain the seasonally aggregated difference
#' between the model/corrected datasets and the training dataset for each pixel.
#' To be used as input in [plt.seasonnal.summary.general()].
#'
#' @author Adrien Michel, 2023 (MeteoSwiss & WSL/SLF)
plt.seasonnal.summary.maps <- function(data.training, data.model,
                                       data.corrected) {

  seasons <- c("Winter","Spring","Summer","Fall")

  # Compute the difference of seasonal mean for each pixel/year
  raw.season.error <- list()
  raw.season.error <- list()
  training <- compute.data.season.mean(data.training)
  raw.season.error[["model"]] <- compute.data.season.mean(data.model)
  raw.season.error[["corrected"]] <-compute.data.season.mean(data.corrected)


  # Compute the absolute error compared to training dataset
  for(season in seasons){
    raw.season.error[["model"]][[season]] <-
      raw.season.error[["model"]][[season]] - training[[season]]
    raw.season.error[["corrected"]][[season]] <-
      raw.season.error[["corrected"]][[season]] - training[[season]]

  }

  # Compute the averaged error for each pixel
  averaged.error <- raw.season.error
  for(season in seasons) {
    for(variant in c("model","corrected")) {
      averaged.error[[variant]][[season]] <- matrix(colMeans(averaged.error[[variant]][[season]], na.rm =T))
    }
  }

  # Plot maps
  par(mfrow=c(4,2),oma=c(0,0,2,0),mar=c(3,3,2,7),mgp=c(1.7,0.5,0))
  legend.lab <- expression(paste(Delta,"SWE (m)"))
  for (season in seasons) {
    lim=max(abs(averaged.error$model[[season]]),abs(averaged.error$corrected[[season]]))
    zlim=c(-lim,lim)

    for(variant in c("model","corrected")) {
      plot(averaged.error[[variant]], var=season, main=paste(variant,season), legend.lab=legend.lab,
           zlim = zlim, legend.line = 2.9, legend.mar = 9, legend.cex = 0.6)
    }
  }
  title("Mean sesonnal SWE error", outer=TRUE)

  return(raw.season.error)
}


#' plt.seasonnal.summary.details
#'
#' \code{plt.seasonnal.summary.details} is a wrapper function for
#' [plt.season.diff()], writing the produced plots to a pdf file
#'
#' @param data.training The training dataset (a vectdata object, see
#' [vectorize.list()])
#' @param data.model The model dataset (a vectdata object, see
#' [vectorize.list()])
#' @param data.corrected the corrected dataset (a vectdata object, see
#' [vectorize.list()])
#' @param out.plot.dir If set, plots are printed to a pdf file in
#' <out.plot.dir>/seasonnal_summary.pdf
#'
#' @return A list containing one list per aggregating function ()
#' Each list  contains a list with entries "model" and "corrected" containing
#' each a list of the four seasons. Seasonal lists contain the seasonally aggregated
#' difference between the model/corrected datasets and the training dataset for
#' each pixel. To be used as input in [plt.season.diff()].
#'
#' @author Adrien Michel, 2023 (MeteoSwiss & WSL/SLF)
#' @export
plt.seasonnal.summary.details <- function(data.training, data.model, data.corrected,
                                          out.plot.dir) {

  if(!is.null(out.plot.dir)){
    pdf(paste0(out.plot.dir,"/seasonnal_summary.pdf"),width=12,height=12)
  }

  seasonnal.summary <- plt.seasonnal.summary.maps(
                         data.training = data.training,
                         data.model = data.model,
                         data.corrected = data.corrected)

  if(!is.null(out.plot.dir)) {
    dev.off()
  }

  return(seasonnal.summary)
}


#' plt.seasonnal.summary
#'
#' \code{plt.seasonnal.summary} produces 6 [barplots()] showing
#' the results returned by [plt.seasonnal.summary.details()]
#'
#' @param seasonnal.summary List returned by [plt.seasonnal.summary.details()]
#'
#' @author Adrien Michel, 2023 (MeteoSwiss & WSL/SLF)
#' @export
plt.seasonnal.summary <- function(seasonnal.summary) {

  seasons <- c("Winter","Spring","Summer","Fall")

  data <- list()
  for(season in seasons) {
    for(variant in c("model","corrected")) {
      data[[variant]][[season]] <- c(seasonnal.summary[[variant]][[season]])
    }
  }

  out <- list()
  for(season in seasons) {
    for(variant in c("model","corrected")) {
      out[[variant]][[season]][["mean"]] <- mean(seasonnal.summary[[variant]][[season]], na.rm=T)
      out[[variant]][[season]][["mae"]] <- mean(abs(seasonnal.summary[[variant]][[season]]), na.rm=T)
    }
  }

  lim <- max(abs(unlist(data)),na.rm=T)
  boxplot(data$model, at = c(1,3,5,7) + 0.05, ylim = c(-lim,lim), xlim = c(0.5,8.5),
          col = c(4,3,7,"chocolate"), xaxt = "n", boxwex = 0.8, main = "Seasonnal summary")
  axis(1, at = c(1.5,3.5,5.5,7.5), label = seasons)
  boxplot(data$corrected, at = c(1,3,5,7)+0.95, add = TRUE,
          col = c(4,3,7,"chocolate"), xaxt = "n", density = 50, boxwex = 0.8)
  legend("topright", legend=c("model","corrected"), col = 1, density = c(NA,50),
         bty = "n", ncol = 2)
  i=0
  for(season in seasons) {
    for(variant in c("model","corrected")) {
      text(1.005+i,-lim,
           paste("Mean\n", round(out[[variant]][[season]][["mean"]], 3), "\n",
                 "MAE\n", round(out[[variant]][[season]][["mae"]], 3)),
           cex=0.5)
      i=i+1
    }
  }

  return(out)
}

################################################################################
####################### CALIBRATION AND VALIDATION #############################
################################################################################
#' plt.seasonnal.summary.maps.calib
#'
#' \code{plt.seasonnal.summary.maps.calib} produces plots of maps of seasonal 
#' averaged bias over the whole area covered by the dataset. Plots are not
#' written to a file. This function can be encapsulated into 
#' [plt.seasonnal.summary.details.calib()])
#'
#' @param data.training.calib The training dataset (a vectdata object, see
#' [vectorize.list()]) for the calibration period
#' @param data.model.calib The model dataset (a vectdata object, see
#' [vectorize.list()]) for the calibration period
#' @param data.corrected.calib the corrected dataset (a vectdata object, see
#' [vectorize.list()]) for the calibration period
#' @param data.training.valid The training dataset (a vectdata object, see
#' [vectorize.list()]) for the validation period
#' @param data.model.valid The model dataset (a vectdata object, see
#' [vectorize.list()]) for the validation period
#' @param data.corrected.valid the corrected dataset (a vectdata object, see
#' [vectorize.list()]) for the validation period
#' 
#' @return A list with entries "calib" and valid", containg a list with entries 
#' "model" and "corrected" containing each a list of the four seasons. 
#' Seasonal lists contain the seasonally aggregated difference between the 
#' model/corrected datasets and the training dataset for each pixel.
#' To be used as input in [plt.seasonnal.summary.general()].
#'
#' @author Adrien Michel, 2023 (MeteoSwiss & WSL/SLF)
plt.seasonnal.summary.maps.calib <- function(data.training.calib, data.model.calib,
                                             data.corrected.calib, data.training.valid,
                                             data.model.valid, data.corrected.valid) {

  seasons <- c("Winter","Spring","Summer","Fall")

  # Compute the difference of seasonal mean for each pixel/year
  raw.season.error <- list()
  raw.season.error[["calib"]] <- list()
  training.calib <- compute.data.season.mean(data.training.calib)
  raw.season.error[["calib"]][["model"]] <- compute.data.season.mean(data.model.calib)
  raw.season.error[["calib"]][["corrected"]] <- compute.data.season.mean(data.corrected.calib)

  raw.season.error[["valid"]] <- list()
  training.valid <- compute.data.season.mean(data.training.valid)
  raw.season.error[["valid"]][["model"]] <- compute.data.season.mean(data.model.valid)
  raw.season.error[["valid"]][["corrected"]] <- compute.data.season.mean(data.corrected.valid)

  # Compute the absolute error compared to training dataset
  for(season in seasons){
    raw.season.error[["calib"]][["model"]][[season]] <-
      raw.season.error[["calib"]][["model"]][[season]] - training.calib[[season]]
    raw.season.error[["calib"]][["corrected"]][[season]] <-
      raw.season.error[["calib"]][["corrected"]][[season]] - training.calib[[season]]
    raw.season.error[["valid"]][["model"]][[season]] <-
      raw.season.error[["valid"]][["model"]][[season]] - training.valid[[season]]
    raw.season.error[["valid"]][["corrected"]][[season]] <-
      raw.season.error[["valid"]][["corrected"]][[season]] - training.valid[[season]]
  }

  # Compute the averaged error for each pixel
  averaged.error <- raw.season.error
  for(season in seasons) {
    for(variant in c("model","corrected")) {
      for(phase in c("calib","valid")) {
        averaged.error[[phase]][[variant]][[season]] <- matrix(colMeans(averaged.error[[phase]][[variant]][[season]], na.rm =T))
      }
    }
  }

  # Plot maps
  par(mfrow=c(4,4),oma=c(0,0,2,0),mar=c(3,3,2,7),mgp=c(1.7,0.5,0))
  legend.lab <- expression(paste(Delta,"SWE (m)"))
  for (season in seasons) {
    lim=max(abs(averaged.error$calib$model[[season]]),abs(averaged.error$calib$corrected[[season]]),
            abs(averaged.error$valid$model[[season]]),abs(averaged.error$valid$corrected[[season]]))
    zlim=c(-lim,lim)

    for(variant in c("model","corrected")) {
      plot(averaged.error$calib[[variant]], var = season, main = paste("Calibration",variant,season),
           legend.lab = legend.lab, zlim = zlim, legend.line = 2.9, legend.mar = 9, legend.cex = 0.6)
    }
    for(variant in c("model","corrected")) {
      plot(averaged.error$valid[[variant]], var = season, main = paste("Validation",variant,season),
           legend.lab = legend.lab, zlim= zlim, legend.line = 2.9, legend.mar = 9, legend.cex = 0.6)
    }
  }
  title("Mean sesonnal SWE error", outer=TRUE)

  return(raw.season.error)
}

#' plt.seasonnal.summary.details.calib
#'
#' \code{plt.seasonnal.summary.details.calib} is a wrapper function for
#' [plt.season.diff.calib()], writing the produced plots to a pdf file
#'
#' @param data.training.calib The training dataset (a vectdata object, see
#' [vectorize.list()]) for the calibration period
#' @param data.model.calib The model dataset (a vectdata object, see
#' [vectorize.list()]) for the calibration period
#' @param data.corrected.calib the corrected dataset (a vectdata object, see
#' [vectorize.list()]) for the calibration period
#' @param data.training.valid The training dataset (a vectdata object, see
#' [vectorize.list()]) for the validation period
#' @param data.model.valid The model dataset (a vectdata object, see
#' [vectorize.list()]) for the validation period
#' @param data.corrected.valid the corrected dataset (a vectdata object, see
#' [vectorize.list()]) for the validation period
#' @param out.plot.dir If set, plots are printed to a pdf file in
#' <out.plot.dir>/seasonnal_summary_calib_valid.pdf
#'
#' @return A list with entries "calib" and valid", containg a list with entries 
#' "model" and "corrected" containing each a list of the four seasons. 
#' Seasonal lists contain the seasonally aggregated difference between the 
#' model/corrected datasets and the training dataset for each pixel.
#' To be used as input in [plt.seasonnal.summary.general()].
#'
#' @author Adrien Michel, 2023 (MeteoSwiss & WSL/SLF)
#' @export
plt.seasonnal.summary.details.calib <- function(data.training.calib, data.model.calib,
                                                data.corrected.calib, data.training.valid,
                                                data.model.valid, data.corrected.valid,
                                                out.plot.dir = NULL) {
  if(!is.null(out.plot.dir)){
    pdf(paste0(out.plot.dir,"/seasonnal_summary_calib_valid.pdf"),width=16,height=12)
  }

  seasonnal.summary <- plt.seasonnal.summary.maps.calib(
                          data.training.calib = data.training.calib,
                          data.model.calib = data.model.calib,
                          data.corrected.calib = data.corrected.calib,
                          data.training.valid = data.training.valid,
                          data.model.valid = data.model.valid,
                          data.corrected.valid = data.corrected.valid)

  if(!is.null(out.plot.dir)) {
    dev.off()
  }
  return(seasonnal.summary)
}

#' plt.seasonnal.summary.calib
#'
#' \code{plt.seasonnal.summary.calib} produces 6 [barplots()] showing
#' the results returned by [plt.seasonnal.summary.details.calib()]
#'
#' @param seasonnal.summary List returned by [plt.seasonnal.summary.details.calib()]
#'
#' @author Adrien Michel, 2023 (MeteoSwiss & WSL/SLF)
#' @export
plt.seasonnal.summary.calib <- function(seasonnal.summary) {

  seasons <- c("Winter","Spring","Summer","Fall")

  data <- list()
  for(season in seasons) {
    for(variant in c("model","corrected")) {
      for(phase in c("calib","valid")) {
        data[[phase]][[variant]][[season]] <- c(seasonnal.summary[[phase]][[variant]][[season]])
      }
    }
  }

  out <- list()
  for(season in seasons) {
    for(variant in c("model","corrected")) {
      for(phase in c("calib","valid")) {
        out[[phase]][[variant]][[season]][["mean"]] <- mean(seasonnal.summary[[phase]][[variant]][[season]], na.rm=T)
        out[[phase]][[variant]][[season]][["mae"]] <- mean(abs(seasonnal.summary[[phase]][[variant]][[season]]), na.rm=T)
      }
    }
  }

  title <- list("calib"="Calibration","valid"="Validation")
  lim <- max(abs(unlist(data)),na.rm=T)
  for(phase in c("calib","valid")) {
    boxplot(data[[phase]]$model, at = c(1,3,5,7) + 0.05, ylim = c(-lim,lim), xlim = c(0.5,8.5),
            col = c(4,3,7,"chocolate"), xaxt = "n", boxwex = 0.8, main = title[[phase]])
    axis(1, at = c(1.5,3.5,5.5,7.5), label = seasons)
    boxplot(data[[phase]]$corrected, at = c(1,3,5,7)+0.95, add = TRUE,
            col = c(4,3,7,"chocolate"), xaxt = "n", density = 50, boxwex = 0.8)
    legend("topright", legend=c("model","corrected"), col = 1, density = c(NA,50),
           bty = "n", ncol = 2)
    i=0
    for(season in seasons) {
      for(variant in c("model","corrected")) {
        text(1.005+i,-lim,
             paste("Mean\n", round(out[[phase]][[variant]][[season]][["mean"]], 3), "\n",
                   "MAE\n", round(out[[phase]][[variant]][[season]][["mae"]], 3)),
             cex=0.5)
        i=i+1
      }
    }
  }

  return(out)
}