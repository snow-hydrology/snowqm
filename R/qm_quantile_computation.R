#' compute.quantiles
#'
#' \code{compute.quantiles} computes the quantile and save the quantile
#' distributions text files for each pixels of the data given as input
#'
#' @param data The training dataset (a `vectData` object, see
#' [vectorize.list()])
#' @param temporal.cluster Temporal groups used to compute the quantile 
#' distributions (see [compute.temporal.cluster])
#' @param spatial.cluster Spatial groups used to compute the quantile 
#' distributions (see [compute.spatial.cluster])
#' @param output.path Directory in which text files of the quantile distributions
#' should be saved
#' @param ncores Number of cores to use for parallel computation, using ncores =
#' 1 defaults to serial computing
#' @param probs The sequence of percentiles [0,1] on which the distribution 
#' should be computed
#'
#' @author Adrien Michel, 2023 (MeteoSwiss & WSL/SLF)
#' @export
compute.quantiles <- function(data, temporal.cluster, spatial.cluster, output.path,
                              ncores=1, probs=seq(0, 1, 0.01))
{

  if(!dir.exists(output.path)) {
    stop(paste0("Output directory '", output.path, "' does not exist"))
  } else if(file.access(output.path, mode=2)==-1) {
    stop(paste0("Output directory '", output.path, "' seems to be not writable"))
  }

  computeQuantilesCpp(swe=data$data,
                      timeCluster=temporal.cluster,
                      spatialCluster=spatial.cluster,
                      probs=probs,
                      xCoords=round(data$x.coords[data$x.indices]),
                      yCoords=round(data$y.coords[data$y.indices]),
                      outputPath=output.path, ncores=ncores)

}

#' train.model
#'
#' \code{train.model} trains the QM model. Meaning that it first computes the
#' spatial and temporal groups (see [compute.temporal.cluster] and 
#' [compute.spatial.cluster]), plots the spatial groups details, then computes
#' the quantile distributions for the training dataset and save them to files
#' (see [compute.quantiles()]), and finally does the same for the model dataset. 
#'
#' @param data.model.file Path the a .RDS file containing the training dataset 
#' (as a vectdata object, see [vectorize.list()])
#' @param data.training.file Path the a .RDS file containing the model dataset 
#' (as a vectdata object, see [vectorize.list()])
#' @param params.qm Parameters for the quantile mapping, a list containing the 
#' following entries: `temporal.step`, `spatial.step`,`elevation.step`, 
#' `slope.step`, `aspect.step`, `curvature.step`, and `target`. See the package 
#' Vignette and the scientific publication for details.
#' @param out.quantiles.dir Path where the quantile distribution files should
#' be saved
#' @param out.plot.dir Path were the plots showing the spatial grouping details
#' should be saved. Directories 'training_quantiles' and 'model_quantiles' will
#' be created
#' @param ncores Number of cores to use for parallel computation, using ncores =
#' 1 defaults to serial computing
#'
#' @author Adrien Michel, 2023 (MeteoSwiss & WSL/SLF)
#' @export
train.model <- function(data.model.file , data.training.file, params.qm,
                        out.quantiles.dir, out.plot.dir, ncores = 1){

  print("[I] Reading model data")
  data.model <- readRDS(data.model.file)
  print("[I] Reading training data")
  data.training <- readRDS(data.training.file)

  print("[I] Creating output directories")
  dir.create(out.quantiles.dir, showWarnings = FALSE, recursive = TRUE)
  dir.create(paste0(out.quantiles.dir,"/model_quantiles/"), showWarnings = FALSE, recursive = TRUE)
  dir.create(paste0(out.quantiles.dir,"/training_quantiles/"), showWarnings = FALSE, recursive = TRUE)
  dir.create(out.plot.dir, showWarnings = FALSE, recursive = TRUE)
  if(length(list.files(paste0(out.quantiles.dir,"/model_quantiles/")))>0) {
    unlink(paste0(out.quantiles.dir,"/model_quantiles/*"))
  }
  if(length(list.files(paste0(out.quantiles.dir,"/training_quantiles/")))>0) {
    unlink(paste0(out.quantiles.dir,"/training_quantiles/*"))
  }

  print("[I] Computing spatial clusters")
  spatial.cluster.full <- compute.spatial.cluster(data = data.model,
                                                params.qm = params.qm,
                                                ncores = ncores)
  spatial.cluster <- lapply(spatial.cluster.full,function(x){x$cluster})

  print("[I] Computing temporal clusters")
  temporal.cluster <- compute.temporal.cluster(data=data.model,
                                             params.qm = params.qm)

  print("[I] Plotting clusters")
  pdf(paste0(out.plot.dir,"/clusters.pdf"),width=14,height=6)
  par(mfrow=c(2,3),oma=c(0,0,0,0),mar=c(3,3,2,7),mgp=c(1.7,0.5,0))
  data.plot=data.model

  data.plot[["total.cluster"]] <- matrix(unlist(lapply(spatial.cluster,length)))
  data.plot[["all.clusters"]] <- matrix(unlist(lapply(spatial.cluster,length)))*
                              min(unlist(lapply(temporal.cluster,length)))
  data.plot[["spatial.cluster"]] <- matrix(unlist(lapply(spatial.cluster.full,
                                                      function(x){x$spatial.cluster})))
  data.plot[["elevation.cluster"]] <- matrix(unlist(lapply(spatial.cluster.full,
                                                         function(x){x$elevation.cluster})))
  data.plot[["slope.cluster"]] <- matrix(unlist(lapply(spatial.cluster.full,
                                                        function(x){x$slope.cluster})))
  data.plot[["aspect.cluster"]] <- matrix(unlist(lapply(spatial.cluster.full,
                                                  function(x){x$aspect.cluster})))
  data.plot[["curvature.cluster"]] <- matrix(unlist(lapply(spatial.cluster.full,
                                                        function(x){x$curvature.cluster})))


  if(min(data.plot[["all.clusters"]])<100) {
    stop(paste0("Pixel ",which.min(data.plot[["all.clusters"]])," has less than 100 points,
                QM impossible."))
  }
  plot(data.plot,var="all.clusters", main=paste0("Cluster sum(min = ",min(data.plot[["all.clusters"]]),")"))
  plot(data.plot,var="dem", main="DEM")
  plot(data.plot,var="spatial.cluster", main="Spatial cluster")
  plot(data.plot,var="elevation.cluster", main="Elevation cluster")
  plot(data.plot,var="slope.cluster", main="Slope cluster")
  plot(data.plot,var="aspect.cluster", main="Aspect cluster")
  plot(data.plot,var="curvature.cluster", main="Curvature cluster")

  dev.off()

  print("[I] Computing model quantiles")
  start.time <- Sys.time()
  compute.quantiles(data.model,
                    temporal.cluster = temporal.cluster,
                    spatial.cluster = spatial.cluster,
                    output.path = paste0(out.quantiles.dir,"/model_quantiles/"),
                    ncores = ncores)
  print(paste("[I] model quantiles computed in",
        as.numeric(round(difftime(Sys.time(),start.time, units = "mins"),2)),
        "minutes"))

  print("[I] Computing training quantiles")
  start.time <- Sys.time()
  compute.quantiles(data.training,
                    temporal.cluster = temporal.cluster,
                    spatial.cluster = spatial.cluster,
                    output.path = paste0(out.quantiles.dir,"/training_quantiles/"),
                    ncores = ncores)
  print(paste("[I] training quantiles computed in",
        as.numeric(round(difftime(Sys.time(),start.time, units = "mins"),2)),
        "minutes"))
  print("[I] Cleaning memory")
  rm(list=ls())
  invisible(gc())
}
