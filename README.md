# SnowQM

SnowQM is an `R` library for correcting snow water equivalent (SWE) grids, called the model data (i.e. the measurements or model output data to be corrected), in order to match another set of SWE grids, the training data (reflecting, for instance, "ground truth data"). The SWE correction is performed by QM. The quantile distributions construction depends on 7 free parameters. The quantile distributions are calculated over a calibration period (training phase of the QM) and are applied to both the calibration and an independent validation period (application of the QM) in order to assess the correction quality. Once trained, the QM can also be applied to the remaining part of the model data not used for calibration and validation.

For more details, see the package vignette and Michel, A., Aschauer, J., Jonas, T., Gubler, S., Kotlarski, S., and Marty, C.: SnowQM 1.0: A fast R Package for bias-correcting spatial fields of snow water equivalent using quantile mapping, Geosci. Model Dev. Discuss. [preprint], https://doi.org/10.5194/gmd-2022-298, in review, 2023

# Installation

> Note for MacOS users: 
> see instruction here about how to enable OpenMP https://github.com/Rdatatable/data.table/issues/5419

### Online installation:

If not already installed, install the `devtools` library:

```
install.packages("devtools")
```
Install SnowQM:

```
devtools::install_git("https://code.wsl.ch/snow-hydrology/snowqm", build_vignettes = TRUE)
```

### Locale installation:

Download the package, then navigate to the package from within `R`:

```
setwd("[...]/SnowQM")
```

If not already installed, install the `devtools` library:

```
install.packages("devtools")
```

Install SnowQM:

```
devtools::install(build_vignettes = TRUE)
```

# Usage
After installation, restart your `R` environment. Then load snowQM using:

```
library(SnowQM)
```

Display the documentation summary using:

```
help(package = 'SnowQM')
```

Use the vignette for a step-by-step example:

```
vignette(package = "SnowQM","snowqm")
```

# About the package documentation
This packages uses C++ implementations for many core function. The documentation
of the package (Rd files) covers only the R functions. C++ functions are documented 
only in their source code. However, they are only called through some documented 
functions of the package (the R C++ wrapper functions are not directly callable
in this package), detailing among other the input parameters.

# Author
Adrien Michel, 2023 (WSL/SLF and MeteoSwiss)

# Citation
Michel, A., Aschauer, J., Jonas, T., Gubler, S., Kotlarski, S., and Marty, C.: SnowQM 1.0: A fast R Package for bias-correcting spatial fields of snow water equivalent using quantile mapping, Geosci. Model Dev. Discuss. [preprint], https://doi.org/10.5194/gmd-2022-298, in review, 2023
