#include <Rcpp.h>
#include <omp.h>
#include <utility>

// User interrupt handling

class interrupt_exception : public std::exception {
  public:
    interrupt_exception(std::string message)
  : detailed_message(message)
  {};
  virtual ~interrupt_exception() throw() {};
  virtual const char* what() const throw() {
    return detailed_message.c_str();
  }
  std::string detailed_message;
};
static inline void check_interrupt_impl(void* /*dummy*/) {
  R_CheckUserInterrupt();
}
inline bool check_interrupt() {
  return (R_ToplevelExec(check_interrupt_impl, NULL) == FALSE);
}

// See comments on spatialClusterInternal below for explainantion about this 
// function
std::pair<std::vector <double>,std::vector <double>> spatialClusterInternalStaticPixel(
                                                        const std::vector<double>& spatial_dist,
                                                        const std::vector<double>& elevation_dist,
                                                        const std::vector<double>& slope_dist,
                                                        const std::vector<double>& aspect_dist,
                                                        const std::vector<double>& curvature_dist,
                                                        const double spatial_position,
                                                        const double elevation_position,
                                                        const double slope_position,
                                                        const double aspect_position,
                                                        const double curvature_position,
                                                        const int = 0,
                                                        const double = 0,
                                                        const double = 0,
                                                        const double = 0,
                                                        const double = 0,
                                                        const double = 0,
                                                        size_t /*count*/ = 0,
                                                        const bool /*plus*/ = false)
{
  const size_t length = spatial_dist.size();
  std::vector<double> indices;
  indices.reserve(length);
  for(size_t i=0; i< length; ++i) {
    if( (spatial_position == 0 || spatial_dist[i] <= spatial_position) &&
        (elevation_position == 0 || elevation_dist[i] <= elevation_position) &&
        (slope_position == 0 || slope_dist[i] <= slope_position) &&
        (aspect_position == 0 || aspect_dist[i] <= aspect_position+0.0001) &&
        (curvature_position == 0 || curvature_dist[i] <= curvature_position) )
    {
      indices.push_back(i+1);
    }
  }
  std::vector<double> params{spatial_position, elevation_position,
                             slope_position, aspect_position, curvature_position};

  return(std::pair<std::vector <double>,std::vector <double>>(indices, params));
}

// See comments on spatialClusterInternal below for explainantion about this 
// function
std::pair<std::vector <double>,std::vector <double>> spatialClusterInternalDynamicPixel(
                                                        const std::vector<double>& spatial_dist,
                                                        const std::vector<double>& elevation_dist,
                                                        const std::vector<double>& slope_dist,
                                                        const std::vector<double>& aspect_dist,
                                                        const std::vector<double>& curvature_dist,
                                                        double spatial_position,
                                                        double elevation_position,
                                                        double slope_position,
                                                        double aspect_position,
                                                        double curvature_position,
                                                        const int target,
                                                        const double spatial_steps,
                                                        const double elevation_steps,
                                                        const double slope_steps,
                                                        const double aspect_steps,
                                                        const double curvature_steps,
                                                        size_t count = 0,
                                                        const bool plus = false) {
  ++count;

  std::pair<std::vector <double>,std::vector <double>> out =
                              spatialClusterInternalStaticPixel(spatial_dist,
                                                                elevation_dist,
                                                                slope_dist,
                                                                aspect_dist,
                                                                curvature_dist,
                                                                spatial_position,
                                                                elevation_position,
                                                                slope_position,
                                                                aspect_position,
                                                                curvature_position,
                                                                target,
                                                                spatial_steps,
                                                                elevation_steps,
                                                                slope_steps,
                                                                aspect_steps,
                                                                curvature_steps);
  const double max_spatial = 100;
  const double max_elevation = 400;
  const double max_slope = 90;
  const double max_aspect = 180;
  const double max_curvature = 0.1;

  bool continue_loop = true;

  if((int)out.first.size() < target){
    continue_loop = false;
    if(!(spatial_position == 0) && spatial_position < max_spatial) {
      spatial_position += spatial_steps;
      continue_loop = true;
    }
    if(!(elevation_position == 0) && elevation_position < max_elevation){
      elevation_position += elevation_steps;
      continue_loop = true;
    }
    if(!(slope_position == 0) && slope_position < max_slope){
      slope_position += slope_steps;
      continue_loop = true;
    }
    if(!(aspect_position == 0) && aspect_position < max_aspect) {
      aspect_position += aspect_steps;
      continue_loop = true;
    }
    if(!(curvature_position == 0) && curvature_position < max_curvature) {
      curvature_position += curvature_steps;
      continue_loop = true;
    }
    // Recursive call until the right amount of pixel are included in the group
    if(continue_loop) {
      out = spatialClusterInternalDynamicPixel(spatial_dist,
                                               elevation_dist,
                                               slope_dist,
                                               aspect_dist,
                                               curvature_dist,
                                               spatial_position,
                                               elevation_position,
                                               slope_position,
                                               aspect_position,
                                               curvature_position,
                                               target,
                                               spatial_steps,
                                               elevation_steps,
                                               slope_steps,
                                               aspect_steps,
                                               curvature_steps,
                                               count,
                                               true);
    }
  }
  else if((int)out.first.size() > target && !plus){
    continue_loop = false;
    if(spatial_position > spatial_steps) {
      spatial_position -= spatial_steps;
      continue_loop = true;
    }
    if(elevation_position > elevation_steps){
      elevation_position -= elevation_steps;
      continue_loop = true;
    }
    if(slope_position > slope_steps){
      slope_position -= slope_steps;
      continue_loop = true;
    }
    if(aspect_position > aspect_steps) {
      aspect_position -= aspect_steps;
      continue_loop = true;
    }
    if(curvature_position > curvature_steps) {
      curvature_position -= curvature_steps;
      continue_loop = true;
    }
    if(continue_loop) {
      out = spatialClusterInternalDynamicPixel(spatial_dist,
                                               elevation_dist,
                                               slope_dist,
                                               aspect_dist,
                                               curvature_dist,
                                               spatial_position,
                                               elevation_position,
                                               slope_position,
                                               aspect_position,
                                               curvature_position,
                                               target,
                                               spatial_steps,
                                               elevation_steps,
                                               slope_position,
                                               aspect_steps,
                                               curvature_position,
                                               count,
                                               true);
    }
  }
  return(out);
}

// Compute the spatial clustering. Returns a list containing, for each
// pixel, a list congaing a vector of indices corresponding to the grouped
// pixels, and the distance used to create the cluster in the spatial, slope,
// elevation, aspect and curvature dimensions
// In the parameter "target" is set to zero, the *_steps parameters are used as
// distance, and all pixels smaller than all distances are grouped using the
// spatialClusterInternalStaticPixel function (above). If "target" is set to 1, 
// the groups congaing only the pixel of interest (i.e. no spatial grouping is 
// applied). If "target" > 1, the distances used start with the *_init
// values, and for each pixels they are increased or decreased dynamically until
// at least "target" number of pixels are in teh group, using the 
// spatialClusterInternalDynamicPixel function. There is an hardcoded upper bound 
// in spatialClusterInternalDynamicPixel function.
Rcpp::List spatialClusterInternal(const std::vector<double>& elevation_cpp,
                                  const std::vector<double>& slope_cpp,
                                  const std::vector<double>& aspect_cpp,
                                  const std::vector<double>& curvature_cpp,
                                  const std::vector<double>& x_coordinates_cpp,
                                  const std::vector<double>& y_coordinates_cpp,
                                  const int target,
                                  double spatial_steps,
                                  double elevation_steps,
                                  double slope_steps,
                                  double aspect_steps,
                                  double curvature_steps,
                                  const double spatial_init,
                                  const double elevation_init,
                                  const double slope_init,
                                  const double aspect_init,
                                  const double curvature_init,
                                  const size_t ncores = 1)
{
  bool interrupt = false;
  const size_t length = elevation_cpp.size();
  std::vector< std::pair <std::vector<double>,std::vector<double> > > out_cpp(length);

  if(target == 1){
    Rcpp::List out(length);
    for(size_t pixel = 0; pixel < length; ++pixel) {
      out[pixel] = Rcpp::List::create(Rcpp::Named("cluster") = Rcpp::wrap(pixel),
                                      Rcpp::Named("spatial.cluster") = 0,
                                      Rcpp::Named("elevation.cluster") = 0,
                                      Rcpp::Named("slope.cluster") = 0,
                                      Rcpp::Named("aspect.cluster") = 0,
                                      Rcpp::Named("curvature.cluster") = 0);
    }
    return(out);
  }

  #pragma omp parallel num_threads(ncores)
  {

    std::vector<double> spatial_dist(length);
    std::vector<double> elevation_dist(length);
    std::vector<double> slope_dist(length);
    std::vector<double> aspect_dist(length);
    std::vector<double> curvature_dist(length);

    std::function<std::pair<std::vector <double>,std::vector <double>> (const std::vector<double>&,
                                                                        const std::vector<double>&,
                                                                        const std::vector<double>&,
                                                                        const std::vector<double>&,
                                                                        const std::vector<double>&,
                                                                        double, double, double, double, double,
                                                                        const int,
                                                                        const double, const double, const double,
                                                                        const double, const double,
                                                                        size_t, const bool)> spatialClusterInternalPixel;
    if(target == 0){
      spatialClusterInternalPixel = spatialClusterInternalStaticPixel;
    } else if (target > 0){
      spatialClusterInternalPixel = spatialClusterInternalDynamicPixel;
    } else{
      throw std::invalid_argument("'Target' must be positive");
    }

    #pragma omp for
    for(size_t pixel = 0; pixel < length; ++pixel) {
      //Check for interrupt
      if (interrupt) {
        continue;
      }
      if (omp_get_thread_num() == 0 && pixel%100 == 0){ // only in master thread!
        if (check_interrupt()) {
          interrupt = true;
        }
      }

      for(size_t i = 0; i < length; ++i){
        spatial_dist[i] = std::sqrt((x_coordinates_cpp[i] - x_coordinates_cpp[pixel])*
          (x_coordinates_cpp[i] - x_coordinates_cpp[pixel]) +
          (y_coordinates_cpp[i] - y_coordinates_cpp[pixel])*
          (y_coordinates_cpp[i] - y_coordinates_cpp[pixel]))/1000;
        elevation_dist[i] = std::abs(elevation_cpp[i] - elevation_cpp[pixel]);
        slope_dist[i] = std::abs(slope_cpp[i] - slope_cpp[pixel]);
        aspect_dist[i] = std::fmod(std::fmod((aspect_cpp[i] - aspect_cpp[pixel] + 180), 360) + 360, 360) - 180;
        curvature_dist[i] = std::abs(curvature_cpp[i] - curvature_cpp[pixel]);
      }

      out_cpp[pixel] = spatialClusterInternalPixel(spatial_dist,
                                                   elevation_dist,
                                                   slope_dist,
                                                   aspect_dist,
                                                   curvature_dist,
                                                   spatial_init,
                                                   elevation_init,
                                                   slope_init,
                                                   aspect_init,
                                                   curvature_init,
                                                   target,
                                                   spatial_steps,
                                                   elevation_steps,
                                                   slope_steps,
                                                   aspect_steps,
                                                   curvature_steps, 0, false);
    }
  }

  if (interrupt) {
    throw interrupt_exception("[I] The computation was interrupted by user");
  }


  Rcpp::List out(length);
  for(size_t pixel = 0; pixel < length; ++pixel) {
    out[pixel] = Rcpp::List::create(Rcpp::Named("cluster") = Rcpp::wrap(out_cpp[pixel].first),
                                    Rcpp::Named("spatial.cluster") = out_cpp[pixel].second[0],
                                    Rcpp::Named("elevation.cluster") = out_cpp[pixel].second[1],
                                    Rcpp::Named("slope.cluster") = out_cpp[pixel].second[2],
                                    Rcpp::Named("aspect.cluster") = out_cpp[pixel].second[3],
                                    Rcpp::Named("curvature.cluster") = out_cpp[pixel].second[4]);
  }
  return(out);
}

// [[Rcpp::export]]
Rcpp::List computeSpatialClusterCpp(const Rcpp::NumericVector elevation,
                                    const Rcpp::NumericVector slope,
                                    const Rcpp::NumericVector aspect,
                                    const Rcpp::NumericVector curvature,
                                    const Rcpp::NumericVector x_coordinates,
                                    const Rcpp::NumericVector y_coordinates,
                                    const int target,
                                    const double spatial_steps,
                                    const double elevation_steps,
                                    const double slope_steps,
                                    const double aspect_steps,
                                    const double curvature_steps,
                                    const double spatial_init,
                                    const double elevation_init,
                                    const double slope_init,
                                    const double aspect_init,
                                    const double curvature_init,
                                    const size_t ncores = 1)
{
  // Convert R object to proper c++ object since R objects are not thread safe
  const std::vector<double> elevation_cpp = Rcpp::as< std::vector<double> >(elevation);
  const std::vector<double> slope_cpp = Rcpp::as< std::vector<double> >(slope);
  const std::vector<double> aspect_cpp = Rcpp::as< std::vector<double> >(aspect);
  const std::vector<double> curvature_cpp = Rcpp::as< std::vector<double> >(curvature);

  const std::vector<double> x_coordinates_cpp = Rcpp::as< std::vector<double> >(x_coordinates);
  const std::vector<double> y_coordinates_cpp = Rcpp::as< std::vector<double> >(y_coordinates);


  return (spatialClusterInternal(elevation_cpp,
                                 slope_cpp,
                                 aspect_cpp,
                                 curvature_cpp,
                                 x_coordinates_cpp,
                                 y_coordinates_cpp,
                                 target,
                                 spatial_steps,
                                 elevation_steps,
                                 slope_steps,
                                 aspect_steps,
                                 curvature_steps,
                                 spatial_init,
                                 elevation_init,
                                 slope_init,
                                 aspect_init,
                                 curvature_init,
                                 ncores));

}