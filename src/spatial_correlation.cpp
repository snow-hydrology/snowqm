#include <Rcpp.h>
#include <omp.h>


// User interrupt handling
static inline void check_interrupt_impl(void* /*dummy*/) {
  R_CheckUserInterrupt();
}
inline bool check_interrupt() {
  return (R_ToplevelExec(check_interrupt_impl, NULL) == FALSE);
}
class interrupt_exception : public std::exception {
public:
  interrupt_exception(std::string message)
    : detailed_message(message)
  {};

  virtual ~interrupt_exception() throw() {};

  virtual const char* what() const throw() {
    return detailed_message.c_str();
  }

  std::string detailed_message;
};

// Returns a list with one entry per pixel, containing the indices of pixels at 
// a distance lower or equal of the distance threshold
// [[Rcpp::export]]
Rcpp::List spatialCorrelationCpp(const Rcpp::NumericVector x_coords,
                                 const Rcpp::NumericVector y_coords,
                                 const size_t ncores,
                                 const double dist_threshold) {

  const size_t size = x_coords.size();

  bool interrupt = false;

  const std::vector<double> x_coords_cpp = Rcpp::as< std::vector<double> >(x_coords);
  const std::vector<double> y_coords_cpp = Rcpp::as< std::vector<double> >(y_coords);

  std::vector< std::vector <size_t> > out_cpp(size);

#pragma omp parallel num_threads(ncores) shared(size, x_coords_cpp, y_coords_cpp, dist_threshold, out_cpp)
#pragma omp for
  for(size_t i = 0; i < size; ++i){
    //Check for interrupt
    if (interrupt) {
      continue;
    }
    if (omp_get_thread_num() == 0){ // only in master thread!
      if (check_interrupt()) {
        interrupt = true;
      }
    }
    std::vector< double > x_dist(size);
    std::vector< double > y_dist(size);
    std::transform(x_coords_cpp.begin(), x_coords_cpp.end(), x_dist.begin(),
                   [&](double x_coord) {return (x_coords_cpp[i]-x_coord)*(x_coords_cpp[i]-x_coord);});
    std::transform(y_coords_cpp.begin(), y_coords_cpp.end(), y_dist.begin(),
                   [&](double y_coord) {return (y_coords_cpp[i]-y_coord)*(y_coords_cpp[i]-y_coord);});
    for(size_t j = 0; j < size; ++j){
      if( ((x_dist[j]+y_dist[j]) > 0) && ((x_dist[j]+y_dist[j]) <= dist_threshold) )
      {
        out_cpp[i].push_back(j+1);
      }
    }
  }
  if (interrupt) {
    throw interrupt_exception("[I] The computation was interrupted by user");
  }

  Rcpp::List out = Rcpp::List::create(out_cpp);

  return(out[0]);
}

// Parallel implementation of row mean computation for matrix
// [[Rcpp::export]]
Rcpp::NumericVector rowMeanCpp(const Rcpp::NumericMatrix data,
                               const size_t ncores) {

  const size_t size = data.nrow();
  const size_t width = data.ncol();

  const std::vector <double> data_cpp = Rcpp::as< std::vector <double> >(data);
  std::vector< double > out_cpp(size,0);

#pragma omp parallel num_threads(ncores) shared(size, width, out_cpp)
#pragma omp for schedule(static, 10)
  for(size_t i = 0; i < size; ++i){
    for(size_t j = 0; j < width; ++j){
      out_cpp[i] += data_cpp[i+j*size];
    }
    out_cpp[i] /= width;
  }

  return(Rcpp::wrap(out_cpp));
}