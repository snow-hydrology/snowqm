#include <Rcpp.h>
#include <omp.h>

// User interrupt handling
static inline void check_interrupt_impl(void* /*dummy*/) {
  R_CheckUserInterrupt();
}
inline bool check_interrupt() {
  return (R_ToplevelExec(check_interrupt_impl, NULL) == FALSE);
}
class interrupt_exception : public std::exception {
public:
  interrupt_exception(std::string message)
    : detailed_message(message)
  {};
  virtual ~interrupt_exception() throw() {};
  virtual const char* what() const throw() {
    return detailed_message.c_str();
  }
  std::string detailed_message;
};

// Movig average function, ma_value is the window size
void ma(std::vector<double>& vect, const int ma_value) {
  const std::vector<double> vect_copy{vect};
  const int size = vect.size();
  const int lag = (ma_value/2);
  for(int i = 0; i < size; ++i) {
    for(int l = -lag; l <= lag; ++l) {
      if(l==0) continue;
      int pos = i-l;
      if(pos < 0) {
        pos+=size;
      }
      vect[i] += vect_copy[pos % size];
    }
  }
  transform(vect.begin(), vect.end(), vect.begin(),
            [ma_value](double c){ return c/ma_value; });
}

// Retunrs the snow season length for the given input vector containing yearly time series
double SeasonLength(const std::vector<double>& vect, const double swe_threshold) {
  std::vector<size_t> results;
  // Get all indices of elements <= swe_threshold
  auto it = std::find_if(std::begin(vect), std::end(vect),
                         [swe_threshold](double c){return c <= swe_threshold;});
  while (it != std::end(vect)) {
    results.push_back(std::distance(std::begin(vect), it));
    it = std::find_if(std::next(it), std::end(vect),
                      [swe_threshold](double c){return c <= swe_threshold;});
  }
  // Only the added 0 at the end is found
  if(results.size()==1){return(vect.size()-1);}
  // Get consecutive difference between indices found (i.e. # snow covered days)
  std::vector<size_t> length(results.size()-1);
  std::transform(results.begin()+1,results.end(),results.begin(),length.begin(),
                 std::minus<double>());
  // return the max, -1 because they all get +1
  return(*std::max_element(length.begin(),length.end())-1);
}


// Computes the snow season length for each pixels and each years, returned in
// a matrix
// [[Rcpp::export]]
Rcpp::NumericMatrix snowSeasonLengthCpp(const Rcpp::NumericMatrix data,
                                        const Rcpp::List indices,
                                        const size_t ncores,
                                        const double swe_threshold = 0.005,
                                        const int ma_value = 7) {

  if(ma_value < 0 || ma_value%2 != 1) {
    throw std::invalid_argument("ma_value should be a posiotive odd integer");
  }
  bool interrupt = false;

  // Convert R object to proper c++ object since R objects are not thread safe
  const size_t num_pixels=data.nrow();
  const size_t num_years=indices.size();

  std::vector< std::vector<size_t> > indices_cpp(num_years);
  for(size_t i=0; i< num_years; ++i)
  {
   indices_cpp[i]=Rcpp::as< std::vector<size_t> >(indices[i]);
  }

  const std::vector<double> data_cpp = Rcpp::as< std::vector<double> >(data);

  std::vector< size_t  > out_cpp(num_pixels*num_years);

  std::vector<double> snow_height(366);

#pragma omp parallel num_threads(ncores) shared(interrupt, data_cpp, indices_cpp, out_cpp, num_pixels, num_years) private(snow_height)
#pragma omp for
  for(size_t i = 0; i < num_pixels; ++i){
    //Check for interupt
    if (interrupt) {
      continue;
    }
    if (omp_get_thread_num() == 0 && i%100==0){ // only in master thread!
      if (check_interrupt()) {
        interrupt = true;
      }
    }
    for(size_t year = 0; year < num_years; ++year) {
      const size_t size = indices_cpp[year].size();
      snow_height.clear();
      snow_height.resize(size);
      size_t k = 0;
      for(const auto indice : indices_cpp[year])
      {
        snow_height[k] = data[i+(indice-1)*num_pixels];
        ++k;
      }
      // Moving average
      ma(snow_height, ma_value);
      //Add a 0 in the end to force the count to stop
      snow_height.push_back(0);
      //Compute season length
      out_cpp[i+year*num_pixels] = SeasonLength(snow_height, swe_threshold);
    }
  }
  if (interrupt) {
    throw interrupt_exception("[I] The computation was interrupted by user");
  }

  Rcpp::NumericMatrix out(num_pixels, num_years, out_cpp.begin());
  Rcpp::CharacterVector out_names = Rcpp::as<Rcpp::CharacterVector>(indices.names());
  colnames(out) = out_names;

  return(out);

}
