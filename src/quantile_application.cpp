#include <Rcpp.h>
#include <omp.h>
#include <fstream>

// User interrupt handling
class interrupt_exception : public std::exception {
public:
  interrupt_exception(std::string message)
    : detailed_message(message)
  {};
  virtual ~interrupt_exception() throw() {};
  virtual const char* what() const throw() {
    return detailed_message.c_str();
  }
  std::string detailed_message;
};
static inline void check_interrupt_impl(void* /*dummy*/) {
  R_CheckUserInterrupt();
}
inline bool check_interrupt() {
  return (R_ToplevelExec(check_interrupt_impl, NULL) == FALSE);
}

// Read quantile text file
bool getFileContent(const std::string fileName,
                    std::vector< std::vector <double>>&  matrix,
                    size_t nrows)
{
  // Open the File
  std::ifstream file(fileName.c_str());
  if(!file)
  {
    return false;
  }
  // Check number of lines
  const size_t row_counts =  std::count(std::istreambuf_iterator<char>(file),
                                        std::istreambuf_iterator<char>(), '\n');
  if(row_counts != nrows) {
    return false;
  }
  // Returns to the beginning of the file
  file.clear();
  file.seekg(0);
  // Check number of columns
  std::string line;
  std::getline(file, line);
  std::stringstream s;
  s << line;
  size_t ncols = 0;
  double summy_value;
  while(s >> summy_value) ++ncols;
  // Returns to the beginning of the file
  file.clear();
  file.seekg(0);
  matrix.resize(nrows);
  for(auto& m : matrix)
    m.resize(ncols);
  for(auto& outer : matrix)
    for(auto& inner : outer)
      file >> inner;
  file.close();
  return true;
}

// Apply the quantile mapping to the data passed as first argument. The "snow" 
// boolean parameters indicates if data contains SWE of another variable (which
// impact of qunatiles of values 0 are treated, see scientific publication). The
// parameter "correct first" indicate if the first timestep should be corrected,
// This is also related to the null value quantile (see scientific publication), 
// becasue for snow melt the previous corrected value might be used.
// [[Rcpp::export]]
Rcpp::NumericVector applyQMCpp(const Rcpp::NumericMatrix swe,
                               const Rcpp::NumericVector doyArray,
                               const Rcpp::NumericVector xCoords,
                               const Rcpp::NumericVector yCoords,
                               const std::string trainingPath,
                               const std::string modelPath,
                               const size_t ncores,
                               const bool snow = true,
                               const bool correctFirst = true)
{
  // Convert R object to proper c++ object since R objects are not thread safe
  const size_t num_doys = 366;
  std::vector<size_t> doyArray_cpp = Rcpp::as< std::vector<size_t> >(doyArray);
  const std::vector<size_t> xCoords_cpp =  Rcpp::as< std::vector<size_t> >(xCoords);
  const std::vector<size_t> yCoords_cpp =  Rcpp::as< std::vector<size_t> >(yCoords);

  const size_t num_pixels=swe.nrow();
  std::vector< size_t > pixels(num_pixels);
  for(size_t i = 0; i < num_pixels; i++) {
    pixels[i] = i;
  }

  if (!snow &&!correctFirst) {
    std::cout << "[W] Parameters 'snow' and 'correct first' are both set to false. Not 'correct first' has only an impact if 'snow==true'";
  }
  std::vector<double> swe_cpp = Rcpp::as< std::vector<double> >(swe);

  bool interrupt = false;
  bool file_io_error = false;
#pragma omp parallel num_threads(ncores) shared(interrupt, pixels, num_doys, num_pixels, swe_cpp, doyArray_cpp, modelPath, trainingPath)
#pragma omp for schedule(static, 5)
  for(const auto p:pixels) {
    //Check for interupt
    if (interrupt | file_io_error) {
      continue;
    }
    if (omp_get_thread_num() == 0){ // only in master thread!
      if (check_interrupt()) {
        interrupt = true;
        continue;
      }
    }
    // Read QM files
    std::vector< std::vector <double>> trainingPDF;
    if(!getFileContent(trainingPath+"/quantiles_"+std::to_string(xCoords_cpp[p])+
       "_"+std::to_string(yCoords_cpp[p])+".txt",
       trainingPDF, num_doys)){
      file_io_error = true;
      std::cout << "Problem reading training quantiles" << std::endl;
      continue;
    }
    std::vector< std::vector <double>> modelPDF;
    if(!getFileContent(modelPath+"/quantiles_"+std::to_string(xCoords_cpp[p])+
       "_"+std::to_string(yCoords_cpp[p])+".txt",
       modelPDF, num_doys)){
      file_io_error = true;
      std::cout << "Problem reading model quantiles" << std::endl;
      continue;
    }
    for(size_t d = 0; d < num_doys; ++d){
      if(modelPDF[d].size() != trainingPDF[d].size()) {
        file_io_error = true;
        std::cout << p << " " << modelPDF[d].size() << " " << trainingPDF[d].size() << std::endl;
        continue;
      }
    }
    for(size_t d = 0; d < doyArray_cpp.size(); ++d) {
      if(d==0 && snow &&!correctFirst) {
        continue;
      } else{
        const size_t doy(doyArray_cpp[d]-1);
        double& target = swe_cpp[p+(d)*num_pixels];
        auto lower = std::upper_bound(modelPDF[doy].begin(), modelPDF[doy].end(), target);
        size_t quantile = std::distance(modelPDF[doy].begin(),lower);
        //Apply the -1 correction and lower bound
        //Here quantiles are 0...100, in R 1...101
        quantile = (quantile <= 1) ? 1 : quantile - 1;
        quantile = (quantile >= (modelPDF[doy].size() - 1)) ? modelPDF[doy].size() - 2 : quantile;
        // Case where snow is in model but not in training --> SWE is simply divided by 2
        if(modelPDF[doy][quantile]==0 && trainingPDF[doy][quantile]>0 && snow) 
        {
          target = d>0 ? swe_cpp[p+(d-1)*num_pixels]/2 : 0;
        }
        else{
          target += trainingPDF[doy][quantile] - modelPDF[doy][quantile];
        }
        // Set a lower limit on SWE
        if(snow) {
          target = target > 0.0005 ? target : 0;
        }
      }
    }
  }
  if (interrupt) {
    throw interrupt_exception("[E] The computation was interrupted by user");
  }
  if(file_io_error){
    throw interrupt_exception("[E] Problem reading QM files, either files do not exist, they do not match the right dimensions, or they are ill formated");
  }
  return Rcpp::wrap(swe_cpp);
}