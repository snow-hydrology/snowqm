#include <Rcpp.h>

// Internal functions, used to choose which seasonal metrics should be computed
double mean_snow(const std::vector<double>::const_iterator first, size_t size, double swe_threshold = 0.005) {
  return(std::accumulate(first, first + size, 0.0)/(size));
}
double max_snow(const std::vector<double>::const_iterator first, size_t size, double swe_threshold = 0.005) {
  return(*std::max_element(first, first + size));
}
double num_snow_days(const std::vector<double>::const_iterator first, size_t size, double swe_threshold = 0.005) {
  return(std::count_if(first, first + size,
                       [=](const double val){return val > swe_threshold;}));
}

// Returns a matrix of size number of year x number of pixels, giving for 
// each pixel and each year, the mean snow, max snow, or number of snow days
// [[Rcpp::export]]
Rcpp::NumericMatrix computeSeasonnalSummaryCpp(const Rcpp::NumericMatrix swe,
                                            const std::string fun,
                                            const Rcpp::List indices,
                                            const double swe_threshold = 0.005)
{
  const size_t num_pixels=swe.nrow();

  const size_t num_years=indices.size();
  std::vector< std::vector<size_t> > indices_cpp(num_years);

  for(size_t i=0; i< num_years; ++i)
  {
    indices_cpp[i]=Rcpp::as< std::vector<size_t> >(indices[i]);
  }

  Rcpp::NumericMatrix out(num_years, num_pixels);

  std::function<double(const std::vector<double>::const_iterator, size_t, double)> fun_cpp;

  if(fun == "mean.snow"){
    fun_cpp = mean_snow;
  } else if(fun == "max.snow"){
    fun_cpp = max_snow;
  } else if(fun == "num.snow.days"){
    fun_cpp = num_snow_days;
  }else {
    throw std::invalid_argument("[I] The function provided ("+fun+") does not exist");
  }
  std::vector<double> vect;
  for(size_t i = 0; i < num_pixels; i++) {
    vect = Rcpp::as< std::vector<double> >(static_cast<Rcpp::NumericVector>(swe.row(i)));
    for(size_t year = 0; year < num_years; ++year) {
      std::vector<double>::const_iterator first = vect.begin() + indices_cpp[year].front() - 1;
      out(year,i) = fun_cpp(first, indices_cpp[year].size(), swe_threshold);
    }
  }
  return(out);
}