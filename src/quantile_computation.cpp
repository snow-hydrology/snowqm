#include <Rcpp.h>
#include <omp.h>
#include <fstream>

// User interrupt handling
class interrupt_exception : public std::exception {
public:
  interrupt_exception(std::string message)
    : detailed_message(message)
  {};
  virtual ~interrupt_exception() throw() {};
  virtual const char* what() const throw() {
    return detailed_message.c_str();
  }
  std::string detailed_message;
};
static inline void check_interrupt_impl(void* /*dummy*/) {
  R_CheckUserInterrupt();
}

inline bool check_interrupt() {
  return (R_ToplevelExec(check_interrupt_impl, NULL) == FALSE);
}

// Quantile function implementation, same algorithm as the R implementation
inline double Lerp(double v0, double v1, double t)
{
  return (1 - t)*v0 + t*v1;
}
std::vector<double> quantile_cpp(const std::vector<double>& inData,
                                 const std::vector<double>& probs)
{
  if (inData.size()==0) {
    return std::vector<double>();
  }
  if (inData.size()==1) {
    return std::vector<double>(1, inData[0]);
  }
  std::vector<double> data = inData;
  std::sort(data.begin(), data.end());
  std::vector<double> quantiles(probs.size());

  for (size_t i = 0; i < probs.size(); ++i)
  {
    const double poi = Lerp(-0.5, data.size() - 0.5, probs[i]);

    const int64_t left = std::max(static_cast<int64_t>(std::floor(poi)),
                                  static_cast<int64_t>(0));
    const int64_t right = std::min(static_cast<int64_t>(std::ceil(poi)),
                                   static_cast<int64_t>(data.size() - 1));

    const double datLeft = data.at(left);
    const double datRight = data.at(right);

    const double quantile = Lerp(datLeft, datRight, poi - left);

    quantiles[i]=quantile;
  }

  return quantiles;
}

// Removes NaN in the input vector
template <typename T>
void remove_nan(std::vector<T>& inData)
{
  inData.erase(std::remove_if(std::begin(inData),
                              std::end(inData),
                              [](const T& value) { return std::isnan(value); }),
                              std::end(inData));
}

// Computes the quantile distribution for the given dataset, using the given
// spatial and temporal grouping, and stores the quantile distributions in text 
// files 
// [[Rcpp::export]]
void computeQuantilesCpp(const Rcpp::NumericMatrix swe,
                             const Rcpp::List timeCluster,
                             const Rcpp::List spatialCluster,
                             const Rcpp::NumericVector probs,
                             const Rcpp::NumericVector xCoords,
                             const Rcpp::NumericVector yCoords,
                             const std::string outputPath,
                             const size_t ncores)
{
  // Convert R object to proper c++ object since R objects are not thread safe
  const size_t num_doys = 366;
  const std::vector<size_t> xCoords_cpp =  Rcpp::as< std::vector<size_t> >(xCoords);
  const std::vector<size_t> yCoords_cpp =  Rcpp::as< std::vector<size_t> >(yCoords);
  const std::vector<double> probs_cpp = Rcpp::as<std::vector<double> >(probs);
  const size_t num_pixels=swe.nrow();
  std::vector< std::vector<size_t> > spatialCluster_cpp(num_pixels);
  for(size_t i=0; i< num_pixels;i++)
  {
    spatialCluster_cpp[i]=Rcpp::as< std::vector<size_t> >(spatialCluster[i]);
  }
  std::vector< std::vector<size_t> > timeCluster_cpp(num_doys);
  for(size_t i=0; i< num_doys;i++)
  {
    timeCluster_cpp[i]=Rcpp::as< std::vector<size_t> >(timeCluster[i]);
  }
  const std::vector<double> swe_cpp = Rcpp::as< std::vector<double> >(swe);

  bool interrupt = false;

#pragma omp parallel num_threads(ncores) shared(interrupt, probs_cpp, num_pixels, spatialCluster_cpp, timeCluster_cpp, swe_cpp)
#pragma omp for schedule(dynamic, 10)
  for(size_t pixel = 0; pixel < num_pixels; pixel++) {
    //Check for interupt
    if (interrupt) {
      continue;
    }
    if (omp_get_thread_num() == 0) { // only in master thread!
      if (check_interrupt()) {
        interrupt = true;
      }
    }

    const std::vector<size_t>& pixels_indices = spatialCluster_cpp[pixel];
    const size_t num_pooled_pixels = pixels_indices.size();

    std::vector<std::vector<double>> out_cpp(num_doys);

    for(size_t doy = 0; doy < num_doys; doy++) {

      const std::vector<size_t>& time_indices = timeCluster_cpp[doy];
      const size_t num_pooled_timestep = time_indices.size();

      std::vector<double> quantiles_data(num_pooled_pixels*num_pooled_timestep);
      for (size_t i=0; i < num_pooled_pixels; i++) {
        for (size_t j=0; j < num_pooled_timestep; j++) {
          quantiles_data[i+j*num_pooled_pixels] = swe_cpp[(pixels_indices[i]-1) +
                                                          (time_indices[j]-1)*num_pixels];
        }
      }
      remove_nan(quantiles_data);
      out_cpp[doy] = quantile_cpp(quantiles_data,probs_cpp);
    }
    // write data to files
    std::ofstream outFile(outputPath+"/quantiles_"+std::to_string(xCoords_cpp[pixel])+
                          "_"+std::to_string(yCoords_cpp[pixel])+".txt");
    for (const auto &line : out_cpp) {
      for (const auto &e : line) {
        outFile << e << " ";
      }
      outFile << "\n";
    }
  }
  if (interrupt) {
    throw interrupt_exception("[I] The computation was interrupted by user");
  }
}