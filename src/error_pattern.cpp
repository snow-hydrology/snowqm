// #include <Rcpp.h>
// #include <omp.h>
// 
// // User interrupt handling
// static inline void check_interrupt_impl(void* /*dummy*/) {
//   R_CheckUserInterrupt();
// }
// inline bool check_interrupt() {
//   return (R_ToplevelExec(check_interrupt_impl, NULL) == FALSE);
// }
// class interrupt_exception : public std::exception {
// public:
//   interrupt_exception(std::string message)
//     : detailed_message(message)
//   {};
//   virtual ~interrupt_exception() throw() {};
//   virtual const char* what() const throw() {
//     return detailed_message.c_str();
//   }
//   std::string detailed_message;
// };
// 
// inline int getBin(const double data, const double  min_value,
//                      const double  max_value, const double bin_size) {
//   return std::floor((data-min_value)/(max_value-min_value))*(bin_size-1);
// }
// 
// // Computes the difference between the two datasets for each pixel and each
// // timestep
// void computeError(std::vector< double >& data1_cpp,
//                   const std::vector< double >& data2_cpp,
//                   const size_t ncores) {
// 
//   bool interrupt = false;
// //#pragma omp parallel for num_threads(ncores)
//   for(size_t i = 0; i < data1_cpp.size(); ++i) {
//     //Check for interrupt
//     if (interrupt) {
//       continue;
//     }
//     if (omp_get_thread_num() == 0 && i%100 == 0) {// only in master thread!
//       if (check_interrupt()) {
//         interrupt = true;
//       }
//     }
//     data1_cpp[i] = data1_cpp[i] > 0 ?
//                    std::min(1., std::abs(data1_cpp[i]-data2_cpp[i])/data1_cpp[i]) : 0;
//     std::cout << data1_cpp[i] << std::endl;
//   }
//   if (interrupt) {
//     throw interrupt_exception("[I] The computation was interrupted by user");
//   }
// }
// 
// Rcpp::List computeBins(const std::vector<double>& data,
//                        const std::vector<double>& variable,
//                        const double bin_h,
//                        const double bin_v,
//                        const size_t nrow,
//                        const size_t ncores) {
//   
//   bool interrupt = false;
//   std::vector < std::vector< double > > out_cpp(bin_v);
//   for(size_t i = 0; i < bin_v; ++i) {
//     out_cpp[i] = std::vector< double >(bin_h);
//   }
//   const double min_value = *std::min_element(variable.begin(), variable.end());
//   const double max_value = *std::max_element(variable.begin(), variable.end());
//   const double steps = (max_value - min_value) / bin_h;
// 
//   Rcpp::NumericVector bins(bin_h);
//   for(size_t i = 0; i < bin_h; ++i) {
//     bins[i] = (min_value+i*steps);
//   }
// 
// #pragma omp parallel for num_threads(ncores)
//   for(size_t i = 0; i < data.size(); ++i) {
//     //Check for interrupt
//     if (interrupt) {
//       continue;
//     }
//     if (omp_get_thread_num() == 0 && i%100 == 0) {// only in master thread!
//       if (check_interrupt()) {
//         interrupt = true;
//       }
//     }
//     const auto d = data[i];
//     const auto value = variable[i%nrow];
//     const auto loc_h = getBin(value, min_value, max_value, bin_h);
//     const auto loc_v = getBin(d, 0, 1, bin_v);
//     ++out_cpp[loc_v][loc_h];
//   }
//   if (interrupt) {
//     throw interrupt_exception("[I] The computation was interrupted by user");
//   }
// 
//   Rcpp::NumericMatrix out_matrix(bin_v , bin_h);
//   Rcpp::NumericVector sum(bin_h);
// 
//   for(size_t j = 0; j < bin_h; ++j) {
//     for(size_t i = 0; i < bin_v; ++i) {
//       sum[j] += out_cpp[i][j];
//     }
//     for(size_t i = 0; i < bin_v; ++i) {
//       out_matrix(i,j) = out_cpp[i][j]/sum[j];
//     }
//   }
//   double tot_sum = std::accumulate(sum.begin(), sum.end(), 0.);
//   std::transform(sum.begin(), sum.end(),
//                  sum.begin(),
//                  [tot_sum](double a) -> double {
//                    double tmp=0;
//                    if (a>0) {tmp = a/tot_sum;}
//                    return tmp;
//                  });
// 
//   Rcpp::List out = Rcpp::List::create(Rcpp::Named("bins") = bins,
//                                       Rcpp::Named("data") = out_matrix,
//                                       Rcpp::Named("count") = sum);
//   return(out);
// }
// 
// // Returns a list with entries elevation, aspect, slope, and curvature. Each 
// // entry contains a matrix of horizontal bins corresponding to the parameter space 
// // (elevation, aspect, slope, and curvature) and vertical bins, corresponding to
// // relative error (in data2 compared to data1) between 0 and 1. The number in
// // the matrix correspond to the proportion of values in this region of parameter
// // value and relative error value
// // [[Rcpp::export]]
// Rcpp::List errorPatternCpp(const Rcpp::NumericMatrix data1,
//                            const Rcpp::NumericMatrix data2,
//                            const double bin_h,
//                            const double bin_v,
//                            const Rcpp::NumericVector elevation,
//                            const Rcpp::NumericVector slope,
//                            const Rcpp::NumericVector aspect,
//                            const Rcpp::NumericVector curvature,
//                            const size_t ncores) {
// 
//   const size_t nrow = data1.nrow();
// 
//   std::vector<double> data1_cpp = Rcpp::as< std::vector<double> >(data1);
//   const std::vector<double> data2_cpp = Rcpp::as< std::vector<double> >(data2);
//   // Relative error is then stored in data1_cpp
//   computeError(data1_cpp, data2_cpp, ncores);
// 
//   const std::vector<double> elevation_cpp = Rcpp::as< std::vector<double> >(elevation);
//   const std::vector<double> slope_cpp = Rcpp::as< std::vector<double> >(slope);
//   const std::vector<double> aspect_cpp = Rcpp::as< std::vector<double> >(aspect);
//   const std::vector<double> curvature_cpp = Rcpp::as< std::vector<double> >(curvature);
// 
//   Rcpp::List out_elevation = computeBins(data1_cpp, elevation_cpp, bin_h, bin_v, nrow, ncores);
//   Rcpp::List out_slope = computeBins(data1_cpp, slope_cpp, bin_h, bin_v, nrow, ncores);
//   Rcpp::List out_aspect = computeBins(data1_cpp, aspect_cpp, bin_h, bin_v, nrow, ncores);
//   Rcpp::List out_curvature = computeBins(data1_cpp, curvature_cpp, bin_h, bin_v, nrow, ncores);
//   Rcpp::List out = Rcpp::List::create(Rcpp::Named("elevation") = out_elevation,
//                                       Rcpp::Named("slope") = out_slope,
//                                       Rcpp::Named("aspect") = out_aspect,
//                                       Rcpp::Named("curvature") = out_curvature);
//   return(out);
// }
